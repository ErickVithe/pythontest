import flask
from flask import request, jsonify
from pymongo import MongoClient
import string
import random
from marvel import Marvel
 
app = flask.Flask(__name__)
app.config["DEBUG"] = True

m = Marvel("a74024ce1c92f51136aeed89bb1068a4",
           "cbd593f496b7bde8214f107a04ee6d5b6847ccfc")

personajes = m.characters 

comics = m.comics 

def searchComic(param):
    results = []
    response = comics.all(title=param)["data"]["results"]
    for comic in response:
        imagen = comic["thumbnail"]["path"] + "." + comic["thumbnail"]["extension"]
        val = {"id": comic["id"], "title": comic["title"], "image": imagen, "onSaleDate": comic["dates"][0]["date"] }
        results.append(val)
    return results

def searchPersonaje(param):
    results = []
    response = personajes.all(name=param)["data"]["results"]
    for char in response:
        id = char["id"]
        imagen = char["thumbnail"]["path"] + "." + char["thumbnail"]["extension"]
        name = char["name"]
        appearances = char["comics"]["available"]
        val = {"id": id, "name": name, "image": imagen, "appearances": appearances }
        results.append(val)
    return results

def searchAllPersonaje():
    results = []
    response = personajes.all()["data"]["results"]
    for char in response:
        id = char["id"]
        imagen = char["thumbnail"]["path"] + "." + char["thumbnail"]["extension"]
        name = char["name"]
        appearances = char["comics"]["available"]
        val = {"id": id, "name": name, "image": imagen, "appearances": appearances }
        results.append(val)
    return results

@app.route('/searchComics', methods=['POST'])
def api_comics():
    res = []
    search = request.json['search']
    tipo = request.json['tipo']

    if tipo:
        if tipo == "comic":
            if search:
                res = searchComic(search)
        elif tipo == "personaje":
            if search:
                res = searchPersonaje(search) 
    else:
        if search:
            res.append(searchComic(search))
            res.append(searchPersonaje(search))
        else:
            res = searchAllPersonaje()

    if len(res)==0:
        res.append("No existen registros")
    return jsonify(res)

@app.route('/users', methods=['POST'])
def api_users():
    name = request.json['name']
    age = request.json['age']
    user = request.json['user']
    password = request.json['pass']

    client = MongoClient("mongodb+srv://evithe:AA11aa..@cluster0.mhapu.mongodb.net/?retryWrites=true&w=majority")
    mydb = client["sample_guides"]
    mycol = mydb["users"]

    myquery = { "user": user, "pass": password }
    count = mycol.count_documents(myquery)
    if count != 0:
        return jsonify("Usuario ya registrado")
    else:
        data = [ {"name": name, "age": age, "user": user, "pass": password} ]
        result = mycol.insert_many(data)
        client.close()
        if result:
            return jsonify("Usuario registrado")
        else:
            return jsonify("Algo falló")


@app.route('/users/login', methods=['POST'])
def api_users_login():
    user = request.json['user']
    password = request.json['pass']

    myclient = MongoClient("mongodb+srv://evithe:AA11aa..@cluster0.mhapu.mongodb.net/?retryWrites=true&w=majority")
    mydb = myclient["sample_guides"]
    mycol = mydb["users"]

    myquery = { "user": user, "pass": password }

    mydoc = mycol.find(myquery)
    res = []

    for x in mydoc:
        token = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(20))
        val = {"id": str(x['_id']), "name": x['name'], "age": x['age'], "token": token }
        res.append( val )
    
    if len(res) != 0:
        return jsonify(res)
    else:
        return jsonify("Usuario no existe") 

 
app.run()